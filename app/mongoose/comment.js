import mongoose from 'mongoose';

const CommentSchema = new mongoose.Schema({
    title: String,
    desc: String
}, {collection:"commentsManager"})

export const Comment = mongoose.model('Comment', CommentSchema);

export const getAllComments = () => 
    new Promise((resolve, reject) => {
        Comment.find((err, comments) => {
            err ? reject(err) : resolve(comments);
        })
    })

export const getComment= (root, {id}) =>
    new Promise((resolve, reject) => {
        Comment.findById(id, (err, comment) => {
            err ? reject(err) : resolve(comment); 
        })
    })

export const addComment= (root, {title, desc}) =>
    new Promise((resolve, reject) => {
        let comment = new Comment;
        comment.title = title;
        comment.desc = desc;
        comment.save((err) => {
            err ? reject(err) : resolve(comment);
        })
    })

export const updateComment= (root, {id, title, desc}) =>
    new Promise((resolve, reject) => {
        Comment.findById(id, (err, comment) => {
            comment.title = title;
            comment.desc = desc;
            comment.save((err) => {
                err ? reject(err) : resolve(comment);
            })

        })
    })

export const deleteComment= (root, {id}) =>
    new Promise((resolve, reject) => {
        Comment.remove({_id: id},(err) => {
            err ? reject(err) : resolve({_id:id});
        })
    })
