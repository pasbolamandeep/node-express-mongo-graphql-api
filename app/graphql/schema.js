import {
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLNonNull,
    GraphQLID,
    GraphQLList
} from 'graphql';
import * as commentSchema from '../mongoose/comment.js'

//comment type
const commentType = new GraphQLObjectType({
    name: 'comment',
    description: 'comment object',
    fields: () => ({
        _id: {
            type: new GraphQLNonNull(GraphQLID)
        },
        title: {
            type: new GraphQLNonNull(GraphQLString)
        },
        desc: {
            type: new GraphQLNonNull(GraphQLString)
        }
    })
})

//comment Queries
const commentQueries = {
    comments: {
        type: new GraphQLList(commentType),
        resolve: commentSchema.getAllComments
    },
    comment: {
        type: commentType,
        args: {
            id: {
                type: GraphQLID
            }
        },
        resolve: commentSchema.getComment
    }
}

//comment mutations
const commentMutations = {
    addComment: {
        type: commentType,
        args: {
            title: {
                name: 'title',
                type: new GraphQLNonNull(GraphQLString)
            },
            desc: {
                name: 'desc',
                type: new GraphQLNonNull(GraphQLString)
            }
        },
        resolve: commentSchema.addComment
    },
    updateComment: {
        type: commentType,
        args: {
            id: {
                type: GraphQLID
            },
            title: {
                type: new GraphQLNonNull(GraphQLString)
            },
            desc: {
                type: new GraphQLNonNull(GraphQLString)
            }
        },
        resolve: commentSchema.updateComment
    },
    deleteComment: {
        type: commentType,
        args: {
            id: {
                type: GraphQLID
            }
        },
        resolve: commentSchema.deleteComment
    }
}

//GraphQL RootQuery
const RootQuery = new GraphQLObjectType({
    name: 'query',
    description: 'root query',
    fields: () => ({
        comments: commentQueries.comments,
        comment: commentQueries.comment,
    })
})

//GrpahQL RootMutation
const RootMutation = new GraphQLObjectType({
    name: 'mutation',
    description: 'root mutation',
    fields: () => ({
        addComment: commentMutations.addComment,
        updateComment: commentMutations.updateComment,
        deleteComment: commentMutations.deleteComment
    })
})

export default new GraphQLSchema({
    query: RootQuery,
    mutation: RootMutation
})
