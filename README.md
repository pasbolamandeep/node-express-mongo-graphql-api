# Node Express MongoDB GraphQL API
Simple GraphQL server to handle CURD operations
### Install Dependencies
[MongoDB](https://docs.mongodb.com/manual/administration/install-community/)
```sh
npm install
```
### Create MongoDB and update the url in index.js
```sh
index.js is pointing to : mongodb://127.0.0.1:27017/commentsManager
```
### Run Server
```sh
npm run start
```
### GrpahQL Endpoint
```sh
http://localhost:1010/graphql
```

#### Get all comments
```sh
query {
  comments {
    _id,
    title,
    desc
  }
}
```
#### Get single comment
```sh
query {
  comment(id:"<_id of the comment>") {
    title,
    desc
  }
}
```
#### Add new comment
```sh
mutation {
  addComment(title:"<title for the comment>", desc: "<description for the comment>"){
    _id
  }
}
```
#### Update comment
```sh
mutation {
  updateComment(id:"<_id of the comment>",title:"<title for the comment>", desc: "<description for the comment>"){
    _id
  }
}
```
#### Delete comment
```sh
mutation {
  deleteComment(id:"<_id of the comment>"){
    _id
  }
}
```