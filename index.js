import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import graphqlHTTP from 'express-graphql';
import schema from './app/graphql/schema';

const app = express();
const port = process.env.PORT || 1010;

mongoose.connect('mongodb://127.0.0.1:27017/commentsManager');

app.use('/graphql', graphqlHTTP( req => ({
    schema,
    pretty: true,
    graphiql: true
})));

app.listen(port);
console.log("API running on port "+port)